
const Menu =
{
    setup()
    {
        const url = `${window.location.origin}/download`;

        return { url };
    },

    template:`
        <span id="menu">

            <img id="logo" src="./assets/logo.svg">

            <span id="menu-right">

                <a id="contact" class="button" href="mailto:gundersena@crimata.com">
                    Contact
                </a>

                <!-- <div id="download" class="button">v2 Coming Soon</div> -->
                <a id="download" class="button" :href="url">Download Preview for Mac</a>

            </span>

        </span>`
}

export default Menu;
